<?php
header("Access-Control_Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$servername = "localhost";
$username = "admin";
$password = "HYOt3qwGikc9";
$dbname = "umbrello";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id, boardid, title, description, status FROM cards ORDER BY id";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
  $cards = array();
  while($row = $result->fetch_assoc()) {
    $card = array(
      "id" => $row["id"],
      "boardid" => $row["boardid"],
      "title" => $row["title"],
      "description" => $row["description"],
      "status" => $row["status"]
    );
    array_push($cards, $card);
  }

  http_response_code(200);
  echo json_encode($cards);
} else {
  http_response_code(404);
  echo json_encode(
    array("message" => "No cards found.")
  );
}

?>
