<?php
$servername = "localhost";
$username = "admin";
$password = "HYOt3qwGikc9";
$dbname = "umbrello";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
} else {
  echo "Connection succeeded!<br><br>";
}

echo "<hr>";
echo "boards table: <br><br>";
$sql = "SELECT id, name, description FROM boards";
//echo "Query is: " . $sql . "<br>";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  echo "<table cellpadding=5 cellspacing=2 border=1><tr><th>id</th><th>name</th><th>description</th></tr>";
  while($row = $result->fetch_assoc()) {
    echo "<tr>";
    echo "<td>" . $row[id] . "</td><td>" . $row["name"] . "</td><td>" . $row["description"] . "</td>";
    echo "</tr>";
  }
  echo "</table>";
} else {
  echo "There were no results from the query.";
}
echo "<br><hr>";

echo "cards table: <br><br>";
$sql = "SELECT id, boardid, title, description, status FROM cards";
//echo "Query is: " . $sql . "<br>";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  echo "<table cellpadding=5 cellspacing=2 border=1><tr><th>id</th><th>boardid</th><th>title</th><th>description</th><th>status</th></tr>";
  while($row = $result->fetch_assoc()) {
    echo "<tr>";
    echo "<td>" . $row[id] . "</td><td>" . $row["boardid"] . "</td><td>" . $row["title"] . "</td><td>" . $row["description"] . "</td><td>" . $row["status"] . "</td>";
    echo "</tr>";
  }
  echo "</table>";
} else {
  echo "There were no results from the query.";
}

$conn->close();
?>