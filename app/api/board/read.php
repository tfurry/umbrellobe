<?php
header("Access-Control_Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$servername = "localhost";
$username = "admin";
$password = "HYOt3qwGikc9";
$dbname = "umbrello";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id, name, description FROM boards ORDER BY id";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
  $boards = array();
  while($row = $result->fetch_assoc()) {
    $board = array(
      "id" => $row["id"],
      "name" => $row["name"],
      "description" => $row["description"]
    );
    array_push($boards, $board);
  }

  http_response_code(200);
  echo json_encode($boards);
} else {
  http_response_code(404);
  echo json_encode(
    array("message" => "No boards found.")
  );
}

?>
