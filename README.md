# README #

Quick setup instructions for Umbrello backend.

### How do I get set up? ###

Assumes you have git and Docker Desktop installed.

* Create a new folder somewhere.
* Open a terminal and CD to the new folder.
* Clone the backend repo into the new folder: “git clone https://tfurry@bitbucket.org/tfurry/umbrellobe.git”. This will pull down about 450 files (none are very large) into a subfolder named ‘umbrellobe’.
* CD into the ‘umbrellobe’ subfolder.
* Verify that there are two new subfolders ‘app’ and ‘mysql’ inside ‘umbrellobe’.
* While still in the ‘umbrellobe’ folder, create and run the docker container with:

  docker run -i -t -p "80:80" -v ${PWD}/app:/app -v ${PWD}/mysql:/var/lib/mysql mattrayner/lamp:latest

* This will take a few minutes to pull down a prebuilt LAMP image, fire it up, attach two file volumes to it (the ‘app’ and ‘mysql’ folders from the git repo), start up the Apache webserver at 0.0.0.0, and preload the MySQL content and state from the ‘mysql’ subfolder.
* When the output logs reach “mysqld entered RUNNING state…”, the container is running (runs in the foreground of the terminal for now).
* Open a browser to http://0.0.0.0/dbcontent.php - this is a raw data dump from the two database tables (boards and cards).
* Open a browser to http://0.0.0.0/api/board/read.php - this is a call to the board entity READ (GET) endpoint - it should return JSON for 2 boards in the database.
* Open a browser to http://0.0.0.0/api/card/read.php - this is a call to the card entity READ (GET) endpoint - it should return JSON for 8 cards in the database.
* Open a browser to http://0.0.0.0/phpmyadmin/index.php - this opens phpMyAdmin for the MySQL database. You can log in using “admin” and “HYOt3qwGikc9”. You can use phpMyAdmin to verify that there is a ‘umbrello’ database with two tables in it. If you like you can modify some of the table records or add new ones and refresh the browsers above to view the changes.
* When finished, close down the docker container by using cntl-C in the terminal.

### Initial design choices for backend: ###
* The website runs from the /app folder as the app's root (recommended by the docker author).
* The REST API is under the /app/api folder to keep it separate from other site files. This allows other web pages to be included in the app if needed (such as dbcontent.php at the root). If the API was placed at the root, the site could not provide other functionality.
* Under /app/api are folders for each entity type (board and card). For each of these there are files for create/delete/read/update to handle the four main REST endpoints (only read files are currently populated). This is enough to provide basic GET functionality for the two entity types.

### Obvious things to do: ###
* Abstract the database connections into a single separate class (I’ve done this before in the distant past but for some reason had difficulty getting it to work this time - IIS provides server errors, I haven’t figured out Apache server errors yet to help debug).
* Update the Apache configuration to allow either .htaccess URL rewrites on the /api folder, or explicit configuration rewrites for calls to the /app/api folder that would allow proper REST formatted URL requests. This would allow me to redirect all calls into this folder to a single front process that would extract the entity type and other request info and route it to the proper entity back process (read/create/delete/update). This is a bit tricky because it involves direct meddling with the Apache configuration file in the container while it’s running (I almost had it working).
* Finish up the four endpoints for each entity type.

### Who do I talk to? ###

* Tim Furry
